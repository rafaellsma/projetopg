Projeto 1 de Processamento Gráfico

- Descrição: o usuário entra via mouse com os pontos de controle de uma curva de Bézier. O número de pontos de controle é arbitrário, sem limite.
O sistema desenha a curva correspondente.  O usuário poderá solicitar a redução do grau da curva (que encontra uma curva aproximada), e ele passa a manipular os novos pontos de controle.  O usuário pode modificar o posicionamento dos pontos, deletar e inserir pontos, e o sistema responder em tempo real de forma adequada, reconstruindo a curva correspondente. O usuário poderá suprimir os pontos de controle, a poligonal de controle, e os pontos da curva. O usuário também poderá determinar o número de avaliações que deverá ser usado para então o sistema calcular os correspondentes pontos da curva e ligá-los por retas. As avaliações deverão ser feitas obrigatoriamente com o Algoritmo de de Casteljau.  Use o método de redução que combina as duas ordens de se percorrer a lista de pontos.

- Dupla: rlsma e bvcl
