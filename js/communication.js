//Arquivo para troca de mensagens entre o bonsaijs e o resto do codigo
function sendEvaluation() {
  var x = document.getElementById("evaluation").value
  movie.sendMessage('receiveEvalution', {
    data: x
  });
}

function sendReduction() {
  var x = 1
  movie.sendMessage('receiveReduction', {
    data: x
  });
}

function selectControlPoint() {
  var x = document.getElementById("controlPoint").checked
  movie.sendMessage('receiveControlPoint', {
    data: x
  });
}

function selectPolygonControl() {
  var x = document.getElementById("polygonControl").checked
  movie.sendMessage('receivePolygonControl', {
    data: x
  });
}

function selectCurveControl() {
  var x = document.getElementById("curveControl").checked
  movie.sendMessage('receiveCurveControl', {
    data: x
  });
}
