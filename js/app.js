var stageObjects = []
var circles = []
var points = []
var controlPoints = []
var curve = null
var controlLines = []

var showControlPolygon = true
var showControlPoints = true
var showCurve = true
var evaluation=1000
//Comunicação entre html e bonsai
//recebe a matriz B e atualiza o necessário
stage.on('message:sendMatrix', function(data) {
          var x = data.data
          //reseta tudo que havia sido feito antes da redução
          points=[]
          circles=[]
          controlPoints=[]
          controlLines=[]
          curve=[]
          //atualiza os arrays que serão usados para fazer a nova distribuição de pontos após a redução da curva
          var m = x['_data']
          for(var i =0; i < m.length;i++){
            points.push(new Point(m[i][0],m[i][1]))
            circles.push(new Circle(m[i][0], m[i][1], 5).attr('fillColor', 'black').on('drag', function(evt){ this.attr({x: evt.x, y:evt.y}); }));
          }
          for(var i=0; i<circles.length-1; i++){
            controlLines.push(new Path([points[i].x,points[i].y,points[i+1].x,points[i+1].y]).stroke('red', 0.9))
          }
          getCurve()
          renderObjects()
});
stage.on('message:receiveEvalution', function(data) {
          evaluation = data.data
          getCurve()
          renderObjects()
});

stage.on('message:receiveReduction', function(data) {
          var num = data.data
          stage.sendMessage('reduction', {
            //envia os pontos e o grau a reduzir (1 grau)
            data:[points,num]
          });
});
//mensagens para mostrar ou não os pontos,o polígono e a curva
stage.on('message:receiveControlPoint', function(data) {
          showControlPoints = data.data
          renderObjects()
});

stage.on('message:receivePolygonControl', function(data) {
          showControlPolygon = data.data
          renderObjects()
});

stage.on('message:receiveCurveControl', function(data) {
          showCurve = data.data
          if(circles.length>2){
            getCurve()
          }
          renderObjects()
});

function interpolation(t, p1, p2){
  var x = (1-t)*p1.x + t*p2.x
  var y = (1-t)*p1.y + t*p2.y
  return new Point(x,y)
}
//vai obter os pontos da curva de acordo  com o Algoritmo de de Casteljau
function getCurve(){
  if(points.length > 2){
    curve = []
    //gerará evaluation pontos de curva
    for (var t = 0; t <= 1; t+=1/evaluation) {
      var pointsTemp = []
      pointsTemp = points
      while (pointsTemp.length>1) {
        var pLenght = pointsTemp.length
        var temp2 = []
        // vai calcular um novo ponto para cada par de pontos n e n+1
        for (var i = 0; i < pLenght-1; i++) {
          var newPoint = interpolation(t, pointsTemp[i], pointsTemp[i+1])
          temp2.push(newPoint)
        }
        //sempre que sai do for, pointsTemp tem um ponto a menos do que tinha anteriormente
        pointsTemp = temp2;
      }
      //quando sai do while, pointsTemp tem apenas 1 ponto, que será o ponto da curva para um certo t
      //adiciona-se esse ponto na curva
      curve.push(pointsTemp[0].x)
      curve.push(pointsTemp[0].y)
      //o primeiro for será executado novamente, com um t diferente para gerar um outro ponto da curva
    }
  }else{
    curve = null
  }
}
//Renderiza os pontos,paths,...
function renderObjects(){
  stageObjects = []
  for (var i = 0; i < controlPoints.length; i++) {
    stageObjects.push(new Circle(controlPoints[i].x,controlPoints[i].y, 5).attr('fillColor', 'blue'))
  }
  if (showCurve && curve) {
    stageObjects.push(new Path(curve).stroke('blue', 0.9))
  }
  if (showControlPolygon) {
    for (var i = 0; i < controlLines.length; i++) {
      stageObjects.push(controlLines[i])
    }
  }
  if(showControlPoints){
    for (var i = 0; i < circles.length; i++) {
      stageObjects.push(circles[i])
    }
  }
  stage.children(stageObjects)
}

stage.on('click', function(e) {
  controlLines = []
  var isExistedCircle = false
  for(var i=0; i<circles.length; i++) {
    if(circles[i] == e.target){
      points[i].x = e.x
      points[i].y = e.y
      isExistedCircle = true
    }
  }
  if(!isExistedCircle){
    circles.push(new Circle(e.x, e.y, 5).attr('fillColor', 'black').on('drag', function(evt){
      this.attr({x: evt.x, y:evt.y});
    }));
    points.push(new Point(e.x, e.y))
  }
  if(circles.length > 1){
    for(var i=0; i<circles.length-1; i++){
      controlLines.push(new Path([points[i].x,points[i].y,points[i+1].x,points[i+1].y]).stroke('red', 0.9))
    }
    getCurve()
  }
  renderObjects()
});
//deleta os pontos
stage.on('doubleclick', function(e) {
  controlLines = []
    for(var i=0; i<circles.length; i++) {
      if(circles[i] == e.target){
        circles.splice(i, 1);
        points.splice(i,1)
        getCurve()
      }
    }
    if(circles.length > 1){
      for(var i=0; i<circles.length-1; i++){
        controlLines.push(new Path([points[i].x,points[i].y,points[i+1].x,points[i+1].y]).stroke('red', 0.9))
      }
    }
    renderObjects()
});
